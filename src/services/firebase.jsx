import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyD14qv1N7_N5AaiUnIZB-F0g18PqhRYvIE",
    authDomain: "app-with-database-22872.firebaseapp.com",
    projectId: "app-with-database-22872",
    storageBucket: "app-with-database-22872.appspot.com",
    messagingSenderId: "313695632135",
    appId: "1:313695632135:web:c785c09b20ffc4e742e3a2"
  };

  firebase.initializeApp(config);
  export const db = firebase.database();
import React, { useState } from 'react';
import { Header } from './Header.jsx';
import { Item } from './Item.jsx';

export function Table() {

    const actualLocalStorage = JSON.parse(localStorage.getItem('astronauts')) || [];  
    const [astronautsList, setAstronautsList] = useState(actualLocalStorage);
    const [inputValues, setInputValues] = useState({ name: "", surname: "", date: "", superpower: "", id: "" });

    const handleSubmit = (event) => {
        event.preventDefault();
    }

    const handleFormInputs = (event) => {
        event.preventDefault();
        if (event.target.value == "") {
            setInputValues(event.target.value);
        }
        else
            setInputValues(prevValues => {
                return { ...prevValues, [event.target.name]: event.target.value }
            });
    }

    const getFormData = (formName) => {
        let formInputs = document.getElementById(formName).elements;
        let data = {};
        for (const item of formInputs) {
            data[item.name] = item.value;
        }
        return data; 
    }

    const addNewAstronaut = () => {
        const newAstronaut = getFormData('form'); 
        const id = astronautsList.length === 0 ? 0 : astronautsList[astronautsList.length - 1].id;
        newAstronaut.id = id + 1; 
        actualLocalStorage.push(newAstronaut);
        localStorage.setItem('astronauts', JSON.stringify(actualLocalStorage));
        setAstronautsList(JSON.parse(localStorage.getItem('astronauts')));
        document.getElementById('form').reset();
        setInputValues({});
    }

    const setFormData = () => {
        document.getElementById('name').value = inputValues.name;
        document.getElementById('surname').value = inputValues.surname;
        document.getElementById('date').value = inputValues.date;
        document.getElementById('superpower').value = inputValues.superpower;
    }

    const editAstronaut = (id) => {
        let itemToEdit = astronautsList.find(item => item.id === id);
        setInputValues(itemToEdit);
        setFormData('form');
        document.getElementById('form').reset();
    }

    const saveEditedAstronaut = () => {
        const editedAstronaut = getFormData('form');
        editedAstronaut.id = inputValues.id;
        const idToEdit = editedAstronaut.id;
        const indexToDelete = astronautsList.indexOf(astronautsList.find(item => item.id == idToEdit));
        astronautsList.splice(indexToDelete, 1, editedAstronaut);
        localStorage.setItem('astronauts', JSON.stringify(astronautsList));
        setAstronautsList(JSON.parse(localStorage.getItem('astronauts')));
        document.getElementById('form').reset();
        setInputValues({});
    }

    const deleteAstronaut = (id) => {
        console.log("entering id:" + id);
        const indexToDelete = astronautsList.indexOf(astronautsList.find(item => item.id == id));
        astronautsList.splice(indexToDelete, 1);
        localStorage.setItem('astronauts', JSON.stringify(astronautsList));
        setAstronautsList(JSON.parse(localStorage.getItem('astronauts')));
    }

    const deleteAll = () => {
        localStorage.clear('astronauts');
        console.log("localStorage:" + localStorage.getItem('astronauts'));
        setAstronautsList([]);
        document.getElementById('form').reset();
        setInputValues({});
    }

    return (
        <div className="wrapper">
            <div className="grid">
                <h1>SEZNAM KOSMONAUTŮ</h1>
                <table>
                    <Header />
                    <tbody>
                        {
                            astronautsList.map(astronaut => (
                                <Item name={astronaut.name}
                                    surname={astronaut.surname}
                                    date={new Date(astronaut.date).toLocaleDateString()}
                                    superpower={astronaut.superpower}
                                    key={astronaut.id}
                                    id={astronaut.id}
                                    deleteFunction={deleteAstronaut}
                                    editFunction={editAstronaut} />
                            ))
                        }
                    </tbody>
                </table>

                <form id="form" onSubmit={handleSubmit}>
                    <ul>
                        <li><h2>Přidání/změna kosmonauta</h2></li>
                        <li><label htmlFor="name">Jméno:</label><br /><input type="text" id="name" name="name" value={inputValues.name} onChange={handleFormInputs}></input></li>
                        <li><label htmlFor="surname">Příjmení:</label><br /><input type="text" id="surname" name="surname" value={inputValues.surname} onChange={handleFormInputs}></input></li>
                        <li><label htmlFor="date">Datum narození:</label><br /><input type="date" id="date" name="date" value={inputValues.date} onChange={handleFormInputs}></input></li>
                        <li><label htmlFor="superpower">Superschopnost:</label><br /><input type="textarea" id="superpower" name="superpower" value={inputValues.superpower} onChange={handleFormInputs}></input></li>
                        <button type="submit" onClick={addNewAstronaut}>PŘIDAT</button>
                        <button type="submit" onClick={saveEditedAstronaut}>ULOŽIT ÚPRAVY</button>
                    </ul>
                </form>

                <div className="deleteButton">
                    <button type="submit" onClick={deleteAll}>SMAZAT SEZNAM</button>
                </div>
            </div>
        </div>
    );
}



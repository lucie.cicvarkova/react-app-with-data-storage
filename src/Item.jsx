import React from "react";
import icon from './editicon.svg';

export function Item(props) {

  return (
    <tr>
      <td>{props.name}</td>
      <td>{props.surname}</td>
      <td>{props.date}</td>
      <td>{props.superpower}</td>
      <td><button onClick={() => props.editFunction(props.id)}><img src={icon} className="edit-icon" alt="edit icon" /></button></td>
      <td><button onClick={() => props.deleteFunction(props.id)}><p style={{ color: 'black' }}>X</p></button></td>
    </tr>
  )
}